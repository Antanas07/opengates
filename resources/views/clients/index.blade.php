@extends('layouts.app')
@include('layouts.header')
@section("content")


<div class="container">
	<div class="row">

	<table>
		<th>Vardas</th>
		<th>Elektroninis paštas</th>
		<th>Telefono numeris</th>
		<th>Projekto kaina</th>
		<th>Sukūrimo laikas</th>
		<th>Peržiūrėti</th>
		@foreach($clients->reverse() as $client)
		<tr>
			<td>{{$client->name}}</td>
			<td>{{$client->email}}</td>
			<td>{{$client->phone}}</td>
			<td>{{$client->price}}&euro;</td>
			<td>{{$client->created_at}}</td>
			<td><a href="{{route('clients.show', $client->id)}}" class="btn btn-info">Peržiūrėti</a>
       
        </td>
        @endforeach
		</tr>

	</table>




</div>
</div>
@endsection






















