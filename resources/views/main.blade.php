@extends('layouts.app')
@include('layouts.header')



<div class="container">
@yield('content')

</div>

<div class="main-type">
	<h1>Paslaugos jūsų verslui!</h1>
	 <div id="typed-strings">
        <span>Reikia  <strong>patikimos</strong> paslaugos?</span>
        <p>Nežinai nuo  ko pradėti?</p>
        <p>Išbandyk paslaugą nemokamai.</p>
       
    </div>
	<h2><span id="typed" style="white-space:pre;"></span></h2>


			



</section>
	<section>
		<div class="block">
		<div class="container">
			<div class="row">
				<div class="context">
					<h1>OpenGates - sujungia žmones su paslaugų sektoriumi</h1>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<i class="fa fa-check-circle-o fa-5x" aria-hidden="true"></i>
						<h1>Sutaupysite brangų laiką.</h1>
					

					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-12">
						<i class="fa fa-compress fa-5x" aria-hidden="true"></i>
						<h1>Gausite reikalingos paslaugos kontaktus</h1>
					

					</div>
				</div>

			</div>
		</div>
			
		</div>



	<div class="categories">
	<div class="container">
   
   
    <div class="row">
    <h1>Kategorijos</h1>
    @foreach($menus as $menu)
    <div class="col-md-4 col-sm-6 col-xs-12">      
      <a href="{{url('form/create')}}"><li>{{$menu->title}}</li></a>    
    </div>
    @endforeach
</div>  
</div>
</div>


	
	<section>
		<div class="block2">
			<div class="container">
				<div class="row">
				<h1>Žmonės apie mus kalba</h1>
					<div class="col-md-6 col-sm-6 col-xs-12 col">
						<div class="primary">
						<p>Išbandžiau jūsų teikimą paslaugą, likau malonei nustebinta, operatyviu darbu, bei gautais patikimos įmonės paslaugomis 
						</p>
						<h3>Klientas: Tomas Stravickas</h3>
						</div>

					</div>
					
					<div class="col-md-6 col-sm-6 col-xs-12 col">
					<div class="primary">
						<p>Išbandžiau jūsų teikimą paslaugą, likau malonei nustebinta, operatyviu darbu, bei gautais patikimos įmonės paslaugomis </p>
						<h3>Klientas: Evaldas Dulskis</h3>
					</div>
					</div>
					
				</div>
				
			</div>
		</div>
	</section>






@include('layouts.footer')