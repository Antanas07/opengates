@extends('layouts.app')
@include('layouts.header')
@section('content')


<div class="container">
    <div class="row">
    <div class="form-create">
  @if(isset($form))
    <form  action="{{route('form.update', $form->id)}}" method="POST">
    <input type="hidden" name="_method" value="PUT">
    @else
    <form  action="{{route('form.store')}}" method="POST">
    @endif
        {{ csrf_field() }}
       
            
            @if(isset($form))
            
            <input value="{{ $form->name }}" class="form-control" type="text" name="name" >
            @else
            <label for="name">Vardas</label>
            <input type="text" name="name" class="form-control" >
            @endif
            
      
            @if(isset($form))
            <input value="{{$form->email}}" class="form-control" type="text" name="email" >
            @else
             <label for="email">Elektroninis paštas</label>
            <input  type="text" class="form-control" name="email">
            @endif


               
            @if(isset($form))
            <input value="{{$form->phone}}" class="form-control" type="text" name="phone" >
            @else
            <label for="phone">Telefono numeris:</label>
            <input  type="text" class="form-control" name="phone">
            @endif

              @if(isset($form))
            <input value="{{$form->price}}" class="form-control" type="text" name="price" >
            @else
            <label for="price">Projekto kaina</label>
            <input  type="text" class="form-control" name="price">
            @endif


            @if(! isset($form))
             <label for="description">Norimos paslaugos aprašymas</label>
            <textarea  type="text" class="form-control" name="description" type="text" name="description"></textarea>
            @else

            <textarea class="form-control" value="{{$form->description}}" name="description"  rows="4" cols="50">{{$form->description}}</textarea>
            @endif

            <button  class="btn btn-success" type="submit" name="submit">{{ isset($form) ? 'Edit' : 'Siūsti'}}</button>

    </form>
    @if(isset($form))
    <form action="{{route('form.destroy', $form->id)}}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
        <button >Delete</button>
    </form>
    @endif

  </div>

</div>
</div>

@endsection
