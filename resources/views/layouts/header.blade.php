<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Projektas</title>
<link href="https://fonts.googleapis.com/css?family=Titillium+Web:300" rel="stylesheet">
  <script src="https://use.fontawesome.com/7b640b2e11.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
 

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
     <script src="{{asset('js/typed.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
   
      <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>

<header class="the-header clearfix">
  <div class="container">
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <a  class="menuBtn">
  <span class="lines"></span>
</a>
  <a href="http://localhost/opengates/public"><h1 class="logo">OpenGates</h1></a>
  
  <nav>
   <div class="main-nav">
    <ul>

    {{--    @if (!Auth::guest() && Auth::user()->is_admin) --}}
      <li><a href="{{url('form/create')}}">Užpildyti formą!</a></li>
    {{--   @else
      @endif --}}
      <li><a href="{{url('menus')}}">Kategorijos</a></li>
        <li><a href="{{url('contact')}}">Kontaktai</a></li>

     
     

        <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Prisijungti</a></li>
        <li><a href="{{ url('/register') }}">Registruotis</a></li>

    @else


        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                   @if (!Auth::guest() && Auth::user()->is_admin) 
              <li><a href="{{url('clients')}}">Klientai</a></li>
              @else
              @endif 
                <li>
                    <a href="{{ url('/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout

                    </a>


                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </li>

            </ul>
        </li>
    @endif
    </ul> 
  </div>
  </nav>
  </div>
  </div>
  </div>
</header>
</body>
</html>