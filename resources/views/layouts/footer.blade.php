<footer>
		<div class="main-footer">
			<ul>
				 <li><a href="{{url('form/create')}}">Užpildyti formą!</a></li>
     			<li><a href="{{url('menus')}}">Kategorijos</a></li>
     			<li><a href="{{url('contact')}}">Kontaktai</a></li>
     			
      			
			</ul>
		</div>
		<div class="social-network">
			<ul>
				<li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
		<div class="reserved">
			<h3>All Right reserved 	&copy; 2016</h3>
		</div>


	</footer>


</body>
</html>