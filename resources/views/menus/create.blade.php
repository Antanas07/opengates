@extends('layouts.app')
@include('layouts.header')
@section('content')



<div class="container">
<div class="row">
  @if(isset($menu))
 
    <form class="form-group forms" action="{{route('menus.update', $menu->id)}}" method="POST">
    
    <input type="hidden" name="_method" value="PUT">
    @else
    <form class="form-group" action="{{route('menus.store')}}" method="POST">
    @endif
        {{ csrf_field() }}

            <label for="name">Kategorijos pavadinimas</label>
            
            @if(isset($menu))
            <input value="{{ $menu->title }}" class="form-control" type="text" name="title">
            @else
            <input class="form-control" type="text" name="title">
            @endif
          
           
         
            <button class="btn btn-success" type="submit" name="submit">{{ isset($menu) ? 'Redaguoti' : 'Pridėti'}}</button>
 

    </form>
    @if(isset($menu))
    <form class="form-group" action="{{route('menus.destroy', $menu->id)}}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="DELETE">
        <button class="btn btn-danger">Ištrinti</button>
    </form>
    @endif

</div>   
</div>
@endsection

