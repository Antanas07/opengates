<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function () {
	$menus = \App\Menu::all();
    return view('main', compact('menus'));


});

Route::get('contact', function(){
	return view('contacts.index');
});

Route::resource('/clients', 'ClientController');

Route::resource('/menus', 'MenuController');

Route::resource('/form', 'FormController');


Route::get('kategorijos', function(){
	return view('category');
});

Route::get('login', function(){
	return views('auth.login');
});

Route::get('register', function(){
	return views('auth.register');
});

Auth::routes();
Route::get('/home', 'HomeController@index');

















