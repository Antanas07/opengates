<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('clients')->insert([
          'Name' => 'Tomas',
          'email' => 'Tomas@gmail.com',
          'phone' => '+37062164665',
          'price' => '2000',
          'description' => 'Reikalinga atlikti SEO vidinį ir išorinįoptimizavimą. Mūsų svetainės adresas yra facebook.com ',
          'created_at' => new \DateTime(),
          'updated_at' => new \DateTime()
      ]);
             DB::table('clients')->insert([
          'Name' => 'Dovydas',
          'email' => 'Dovydas@gmail.com',
          'phone' => '+37062164455',
          'price' => '4500',
          'description' => 'Sveiki, ieškome programuotojų komandos, kuri galėtų sukurti restorano rezervacijos sistemą.',
          'created_at' => new \DateTime(),
          'updated_at' => new \DateTime()
      ]);
    }
}
