<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
          'name' => 'admin',
          'email' => 'admin@admin.lt',
          'password' => Hash::make('adminas'),
        
          'is_admin' => 1,
          'created_at' => new \DateTime(),
          'updated_at' => new \DateTime()
      ]);

          DB::table('users')->insert([
          'name' => 'open',
          'email' => 'open@opengates.lt',
          'password' => Hash::make('opengates'),
        
          'is_admin' => 0,
          'created_at' => new \DateTime(),
          'updated_at' => new \DateTime()
      ]);
    }
}
